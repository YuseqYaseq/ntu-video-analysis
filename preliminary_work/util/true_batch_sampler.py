import numpy as np
import pandas as pd
from math import ceil
from torch import nn

class TrueBatchSampler:
    """
    Reads from dataset once using slicing (on shuffle set to False) and
    list of indexes (on shuffle set to True) to speed up the processing.
    The dataset has to implement __len__ and __getitem__.
    """
    def __init__(self, dataset, batch_size, shuffle):
        self.dataset = dataset
        self.batch_size = batch_size
        self.shuffle = shuffle
        if self.shuffle:
            self.idx = np.random.choice(range(len(self.dataset)), size=len(self.dataset), replace=False)
        self.i = 0

    def __iter__(self):
        self.i = 0
        if self.shuffle:
            self.idx = np.random.choice(range(len(self.dataset)), size=len(self.dataset), replace=False)
        return self

    def __next__(self):
        if self.i*self.batch_size < len(self.dataset):
            self.i += 1
            start = (self.i - 1) * self.batch_size
            end = self.i * self.batch_size
            if self.shuffle:
                if isinstance(self.dataset, pd.DataFrame):
                    return self.dataset.iloc[self.idx[start:end]]
                return self.dataset[self.idx[start:end]]
            else:
                if isinstance(self.dataset, pd.DataFrame):
                    return self.dataset.iloc[start:end]
                return self.dataset[start:end]
        else:
            raise StopIteration

    def __len__(self):
        return ceil(len(self.dataset) / self.batch_size)

def create_sequential_from_modules(input_size, output_size, modules=None,
                                   activation_function=nn.ReLU, output_function=None):
    if modules is None or len(modules) == 0:
        layers = [nn.Linear(input_size, output_size)]
    else:
        layers = [nn.Linear(input_size, modules[0]), activation_function()]
        for i in range(1, len(modules)):
            layers.extend([nn.Linear(modules[i - 1], modules[i]), activation_function()])
        layers.append(nn.Linear(modules[-1], output_size))
    if output_function is not None:
        layers.append(output_function())
    return nn.Sequential(*layers)
