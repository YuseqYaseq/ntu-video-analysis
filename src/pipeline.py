from pathlib import Path
from warnings import warn

from functools import partial

import pandas as pd
import torch
#from torch.optim import Adam, lr_scheduler
from transformers import AdamW, get_scheduler, BertForSequenceClassification, \
    RobertaForSequenceClassification, XLNetForSequenceClassification
from torch import nn
from tqdm import tqdm
from uuid import uuid4

from src.models.bert import preprocess_data, get_bert, get_bert_tokenizer
from src.models.roberta import get_roberta
from src.models.xlnet import get_xlnet
from src.util import get_device, TrueBatchSampler, weighted_binary_cross_entropy, ExperimentSetting, \
    NaNLossDetectedException, get_preds, get_metrics, jaccard_index


def get_model_tokenizer(config: ExperimentSetting, device: torch.device):
    """
    builds deep learning model based on config
    """
    if config.selected_model.lower() == 'bert':
        model, tokenizer = get_bert(config, 'bert-base-uncased', device)
    elif config.selected_model.lower() == 'roberta':
        # there was a bug in get_x and so now only get_bert is used, but it actually returns roberta and xlnet accordingly.
        # too lazy to fix this hot mess.
        model, tokenizer = get_bert(config, 'roberta-base', device)
    elif config.selected_model.lower() == 'xlnet':
        model, tokenizer = get_bert(config, 'xlnet-base-cased', device)
    else:
        raise RuntimeError('Unknown model!')
    return model, tokenizer


def training_pipeline(df_train: pd.DataFrame, df_val: pd.DataFrame, config: ExperimentSetting):
    """
    training pipeline for BERT, RoBERTa and XLNet models. Saves results to results folder
    or alternatively path provided in config.
    :param df_train: dataframe containing training data.
    Contains at least columns: 'text' and either 6 (if config.task == 'full')
    or 1, 2, 3, 4, 5 (if config.task == 'simplified')
    :param df_val: dataframe containing validation data. Has the same format as df_train.
    :param config: ExperimentSetting object. Refer to config.py for all fields which this object should contain.
    :returns: dict {'model': trained model,
                    'train_loss',: loss per epoch on df_train
                    'val_loss', loss per epoch on df_val
                    'train_batch_loss', loss per batch on df_train
                    'val_batch_loss', loss per batch on df_val
                    'config': config}
    """
    results = ExperimentSetting()
    try:
        torch.manual_seed(config.seed)
        torch.cuda.manual_seed(config.seed)
        if config.save_path is None:
            config.save_path = Path(f'results/{config.selected_model}_{uuid4()}.pt')
        results.config = config
        device = get_device()
        model, tokenizer = get_model_tokenizer(config, device)
        dataset_train = TrueBatchSampler(df_train, batch_size=config.batch_size, shuffle=True, seed=config.seed)
        dataset_val = TrueBatchSampler(df_val, batch_size=config.batch_size, shuffle=True, seed=config.seed)

        #opt = Adam(model.parameters(), lr=config.lr, weight_decay=config.weight_decay)
        opt = AdamW(model.parameters(), lr=config.lr, weight_decay=config.weight_decay)

        if config.task == 'simplified':
            # liczba elementów klasy 1
            weight1 = df_train[config.full_task_num_of_classes].sum()
            weights = torch.tensor([weight1, len(df_train) - weight1], dtype=torch.float32)
            weights /= weights.sum()
            loss_fn = weighted_binary_cross_entropy
        elif config.task == 'full':
            counts_pos = torch.tensor([0 for _ in range(config.full_task_num_of_classes)]).to(device)
            counts_neg = torch.tensor([0 for _ in range(config.full_task_num_of_classes)]).to(device)
            for i in dataset_train:
                x, actual = preprocess_data(i, config, torch.device('cpu'))
                bs = len(actual)
                actual = torch.sum(actual, dim=0).to(dtype=torch.int32)
                counts_pos += actual.to(device)
                counts_neg += (torch.tensor([bs for _ in range(config.full_task_num_of_classes)]) - actual).to(device)
            loss_fn = nn.BCEWithLogitsLoss(pos_weight=(torch.true_divide(counts_neg, counts_pos)))
        else:
            raise RuntimeError('Unknown task!')

        #scheduler = lr_scheduler.ReduceLROnPlateau(optimizer=opt,
        #                                           factor=config.lr_scheduler_factor,
        #                                           patience=config.lr_scheduler_patience,
        #                                           min_lr=config.lr_scheduler_min_lr,
        #                                           verbose=True)
        scheduler = get_scheduler("linear", optimizer=opt,
                                  num_warmup_steps=0,#1 * len(dataset_train),
                                  num_training_steps=config.epochs * len(dataset_train))

        train_loss = []
        val_loss = []
        train_batch_loss = []
        val_batch_loss = []
        results.train_loss = train_loss
        results.val_loss = val_loss
        results.train_batch_loss = train_batch_loss
        results.val_batch_loss = val_batch_loss
        for e in range(config.epochs):
            train_loss_sum = 0
            train_loss_count = 0
            for batch in tqdm(dataset_train):
                x, y = preprocess_data(batch, config, device)
                x = tokenizer(x, return_tensors='pt', padding=True, truncation=False).to(device)
                if isinstance(model, (BertForSequenceClassification, RobertaForSequenceClassification,
                                      XLNetForSequenceClassification)):
                    pred = model(**x).logits

                    if config.task == 'simplified':
                        pred = torch.sigmoid(pred)
                    else:
                        pred = torch.sigmoid(pred)
                else:
                    pred = model(x)
                if config.task == 'simplified':
                    loss = loss_fn(pred, y, weights=weights)
                else:
                    loss = loss_fn(pred, y)
                if torch.isnan(loss):
                    #results['model_head'] = model.l.state_dict()
                    results['model'] = model.state_dict()
                    raise NaNLossDetectedException()

                opt.zero_grad()
                loss.backward()
                opt.step()
                scheduler.step()

                train_batch_loss.append(loss.item())
                train_loss_sum += loss.item()
                train_loss_count += len(x)
            train_loss.append(train_loss_sum / train_loss_count)

            val_loss_sum = 0
            val_loss_count = 0
            for batch in tqdm(dataset_val):
                x, y = preprocess_data(batch, config, device)
                x = tokenizer(x, return_tensors='pt', padding=True, truncation=False).to(device)
                if isinstance(model, (BertForSequenceClassification, RobertaForSequenceClassification,
                                      XLNetForSequenceClassification)):
                    pred = model(**x).logits
                    pred = torch.sigmoid(pred)
                else:
                    pred = model(x)
                if config.task == 'simplified':
                    loss = loss_fn(pred, y, weights=weights)
                else:
                    loss = loss_fn(pred, y)

                val_batch_loss.append(loss.item())
                val_loss_sum += loss.item()
                val_loss_count += len(x)
            val_loss.append(val_loss_sum / val_loss_count)
            print(f'epoch {e}:\ttrain loss: {train_loss[-1]}\tval loss: {val_loss[-1]}')

            #scheduler.step(val_loss[-1])
            #if e == config.bert_unfreeze_at:
            #    model.unfreeze_bert()

            if config.save_checkpoint:
                checkpoint_path = config.save_path.with_name(config.save_path.stem + f'_checkpoint_{e}.pkl')
                #if e > config.bert_unfreeze_at:
                results['model'] = model.state_dict()
                #else:
                #    results['model_head'] = model.l.state_dict()
                torch.save(results, checkpoint_path)

        #if e > config.bert_unfreeze_at:
        results['model'] = model.state_dict()
        #else:
        #    results['model_head'] = model.l.state_dict()
        torch.save(results, config.save_path)
        return results
    except NaNLossDetectedException:
        warn('NaN Loss detected. Stopping training to preserve weights.')
        torch.save(results, config.save_path)
        return results
    except Exception as exc:
        try:
            #if e > config.bert_unfreeze_at:
            results['model'] = model.state_dict()
            #else:
            #    results['model_head'] = model.l.state_dict()
        except NameError:
            pass
        torch.save(results, config.save_path)
        raise exc


def _add_margin_to_point_metrics(margin, x):
    if x['Behavior type'] == 'POINT':
        x['Start'] -= margin
        x['Stop'] += margin
    return x


def test_pipeline(behavior_df, df_test, model, config, behaviors_mapping,
                  jaccard_index_point_behavior_additional_time=0.1):
    """
    Test pipeline for BERT, RoBERTa and XLNet models on NTU dataset.
    :param behavior_df: dataframe audio_observations_v3. We use it co calculate jaccard index.
    :param df_test: dataframe containing test data.
    Contains at least columns: 'text' and either 6 (if config.task == 'full')
    or 1, 2, 3, 4, 5 (if config.task == 'simplified')
    :param model: trained model. Either BERT, RoBERTa or XLNet.
    :param config: config used to train model.
    :param behaviors_mapping: dict which maps behaviors in behavior_df to [0, 1, 2, 3, 4, 5, 6].
    :param jaccard_index_point_behavior_additional_time: defines how wide are point observations for the purpose of
    jaccard index calculation.
    :return: tuple (true positive, true negative, false positive, false negative, accuracy,
                    precision, recall, f1 score, jaccard index)
    """
    device = get_device()
    _, tokenizer = get_model_tokenizer(config, device)

    if config.task == 'simplified':
        # liczba elementów klasy 1
        used_classes = 1
        intersection, sum = 0, 0
    elif config.task == 'full':
        used_classes = config.full_task_num_of_classes
        intersection, sum = [0 for _ in range(used_classes)], [0 for _ in range(used_classes)]
    else:
        raise RuntimeError('Unknown task!')

    model = model.to(device)
    df_test = df_test.reset_index(drop=True)
    dataset = TrueBatchSampler(df_test, shuffle=False, batch_size=4)

    def preprocess_with_tokenizer(x, config, device):
        x, y = preprocess_data(x, config, device)
        x = tokenizer(x, return_tensors='pt', padding=True, truncation=False).to(device)
        return x, y
    preds, actuals = get_preds(model, dataset, preprocess_with_tokenizer,
                               used_classes, config, device)
    tp, tn, fp, fn, a, p, r, f1 = get_metrics(preds, actuals, used_classes)

    preds = preds.reshape(-1, used_classes)
    for filename, group in behavior_df.groupby('Media file mapped'):
        df_test_ = df_test[df_test['filename'] == filename]
        if len(df_test_) == 0:
            continue
        start_stop_mapper = partial(_add_margin_to_point_metrics,
                                    jaccard_index_point_behavior_additional_time)
        group = group.apply(start_stop_mapper, axis=1)
        group['Behavior'] = group['Behavior'].map(behaviors_mapping)
        if config.task == 'simplified':
            df_test_ = df_test_[df_test_[config.full_task_num_of_classes] == 1.0]
            group = group[group['Behavior'].isin((0, 1))]
            if len(group) == 0:
                continue
            timestamps1 = df_test_[(preds[df_test_.index] >= 0.5).flatten().tolist()][['Start', 'Stop']].values
            timestamps2 = group[['Start', 'Stop']].values
            int, sum_ = jaccard_index(timestamps1, timestamps2,
                                      return_tuple=True)
            intersection += int
            sum += sum_
            ji = intersection / sum
        elif config.task == 'full':
            print('no jaccard index for full task')
            ji = None
        else:
            raise RuntimeError('Unknown task!')

    return tp, tn, fp, fn, a, p, r, f1, ji


def ted_test_pipeline(df_test: pd.DataFrame, model, config: ExperimentSetting):
    """
    Test pipeline for BERT, RoBERTa and XLNet models on TED talks dataset.
    :param df_test: dataframe containing test data.
    Contains at least columns: 'text' and either 6 (if config.task == 'full')
    or 1, 2, 3, 4, 5 (if config.task == 'simplified')
    :param model: trained model. Either BERT, RoBERTa or XLNet.
    :param config: config used to train model. Refer to config.py for possible options.
    :return: tuple (true positive, true negative, false positive, false negative, accuracy,
                    precision, recall, f1 score)
    """
    device = get_device()
    _, tokenizer = get_model_tokenizer(config, device)

    if config.task == 'simplified':
        # liczba elementów klasy 1
        used_classes = 1
    elif config.task == 'full':
        used_classes = config.full_task_num_of_classes
    else:
        raise RuntimeError('Unknown task!')

    model = model.to(device)
    df_test = df_test.reset_index(drop=True)
    dataset = TrueBatchSampler(df_test, shuffle=False, batch_size=4)

    def preprocess_with_tokenizer(x, config, device):
        x, y = preprocess_data(x, config, device)
        x = tokenizer(x, return_tensors='pt', padding=True, truncation=False).to(device)
        return x, y
    preds, actuals = get_preds(model, dataset, preprocess_with_tokenizer,
                               used_classes, config, device)
    tp, tn, fp, fn, a, p, r, f1 = get_metrics(preds, actuals, used_classes)

    return tp, tn, fp, fn, a, p, r, f1
