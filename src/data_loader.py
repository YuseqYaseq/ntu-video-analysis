import os
import pandas as pd
import torch
from pathlib import Path
from warnings import warn
import numpy as np
import librosa


def multilabel_onehot(labels, n_categories, device=None, dtype=torch.float32):
    batch_size = len(labels)
    one_hot_labels = torch.zeros(size=(batch_size, n_categories), dtype=dtype, device=device)
    for i, label in enumerate(labels):
        one_hot_labels[i] = one_hot_labels[i].scatter_(dim=0, index=torch.tensor(label, device=device), value=1.)
    return one_hot_labels


class AudioFeaturesDataset:
    def __init__(self, path_to_audio_files: str, path_to_annotations: str, batch_size: int,
                 frame_length: int, hop_length: int, window_length: int, use_raw_audio: bool,
                 n_mfcc: int = 0, n_mfcc_hop_length: int = 0, selected_videos=None, device=None):
        """
        Audio features dataset. Iterate over it to iterate over all videos.
        Returns x of size (batch_size, window_length) if use_raw_audio is set or
        (batch_size, ceil(window_length / n_mfcc_hop_length), n_mfcc + 20) otherwise.
        Return y of size (batch_size, n_classes) with classes set if an event occurs
        within the frame.
        Currently only uses 'asking questions' behavior
        example:
        frame_length = 2
        hop_length = 1
        window_length = 2 (frame length) + 3 (context to the left) + 3 (context to the right) = 8
        n_classes = 3

        333----------------------- feature 3
        -----2222----------------- feature 2
        ----11-------------------- feature 1
        |||--||| (first iteration) (return y = [1, 0, 0])
         |||--||| (second iteration) (return y = [1, 1, 0])
          |||--||| (third iteration) (return y = [1, 1, 0])
                    ...
                          |||--||| (last iteration) (return y = [0, 0, 0])
        :param path_to_audio_files: Path to audio folder
        :param path_to_annotations: Path to all_observations.tsv
        :param batch_size: batch size
        :param frame_length: length of the frame
        :param hop_length: length of the jump at every iteration
        :param window_length: length of the window
        :param use_raw_audio: True - return raw audio;
                              False - return mfcc, chroma, spectral contrast, spectral center
        :param n_mfcc: How many mfcc?
        :param n_mfcc_hop_length: At every iteration split window into
        ceil(window_length / n_mfcc_hop_length) parts and calculate features for each slice.
        :param selected_videos: dataset will only use videos with filename matches one of the
        filenames in selected_videos. None for all videos in path_to_audio_files.
        Useful when splitting dataset to train/val/test.
        :param device: move returned tensors to device, None for default device
        """
        import librosa
        if not use_raw_audio:
            assert window_length % n_mfcc_hop_length == 0 and frame_length % n_mfcc_hop_length == 0,\
                "window_length and frame_length must be divisible by n_mfcc_hop_length!"
        self.path_to_audio_files = Path(path_to_audio_files)
        self.default_sample_rate = 22050
        self.n_mfcc = n_mfcc
        self.n_mfcc_hop_length = n_mfcc_hop_length
        self.frame_length = frame_length
        self.hop_length = hop_length
        self.window_length = window_length
        self.used_suffix = '.wav'
        self.unrecognized_paths = set()
        self.files = set()
        self.use_raw_audio = use_raw_audio
        self.batch_size = batch_size
        self.device = device
        if not use_raw_audio:
            self.feature_window_size = self.window_length // self.n_mfcc_hop_length + 1
        else:
            self.feature_window_size = 0
        self.missing_annotations = set()

        self.observations = pd.read_csv(path_to_annotations, parse_dates=True)
        self.observations['filename'] = self.observations['Media file mapped'].apply(lambda x: x + self.used_suffix)

        if selected_videos is not None:
            self.observations = self.observations[self.observations['filename'].isin(selected_videos)]

        self.sample_rates = {}
        for filename in os.listdir(self.path_to_audio_files):

            if selected_videos is not None:
                if filename not in selected_videos:
                    continue

            if filename.endswith(self.used_suffix):
                sr = librosa.get_samplerate(self.path_to_audio_files / filename)
                self.sample_rates[filename] = sr
                if not (self.observations['filename'] == filename).any():
                    warn(f'No annotations found for file {filename}. It will NOT be added to the training data.')
                    self.missing_annotations.add(filename)
                else:
                    self.files.add(filename)

        def map_sample_rate(x):
            f = x['filename']
            if f not in self.sample_rates:
                self.unrecognized_paths.add(f)
                return None
            return self.sample_rates[f]

        self.observations['sample_rate'] = self.observations.apply(map_sample_rate, axis=1)
        self.observations = self.observations[~self.observations['sample_rate'].isnull()]
        self.observations['Start (frame)'] = self.observations\
            .apply(lambda x: round(x['Start'] * x['sample_rate']), axis=1)
        self.observations['Stop (frame)'] = self.observations\
            .apply(lambda x: round(x['Stop'] * x['sample_rate']), axis=1)

        # Our target
        features = [
            'Active teacher stands by slides and explains them',
            'Giving questions to students: rhetorical, comprehension questions',
            'Asking questions',
            'Use of voice intonation to emphasise more important issues/topics',
            'Organization: giving class outline, clearly indicating transition from one topic to another',
            'Giving hints how to do something',
            'Summing up',
            'Laughter'
        ]

        self.no_classes = len(features)
        self.observations['Behavior'] = self.observations['Behavior'].apply(self._map_features)
        self.observations = self.observations[self.observations['Behavior'].isin(features)]
        self.observations['target'] = self.observations\
            .apply(lambda x: features.index(x['Behavior']), axis=1)

        frame_offset = (window_length - frame_length) // 2
        self.first_frame_start = frame_offset
        self.first_frame_end = frame_offset + frame_length
        if len(self.unrecognized_paths) > 0:
            warn(f'Some paths were not matched with a file: {list(self.unrecognized_paths)}')

    def __iter__(self):
        for filename in self.files:
            xs, ys = [], []
            for i, (x, y) in enumerate(self._iterate_over_stream(filename)):
                xs.append(torch.tensor(x, device=self.device))
                ys.append(y)
                if i % self.batch_size == self.batch_size - 1:
                    xs, ys = self._cat_and_encode_data(xs, ys)
                    yield xs, ys
                    xs, ys = [], []

            # Check if there are any leftovers (with length less than batch_size)
            # at the end of the file and yield them too.
            if len(xs) > 0:
                # drop last example if it's too small (it means that we're at the end of the audio file and we assume
                # there isn't much going on - this only works for long videos like ntu videos dataset.
                # We don't pad because it would move the frame relatively to the window and mess up predictions.
                if len(xs[-1]) < self.window_length and self.use_raw_audio:
                    # pad = torch.nn.ConstantPad1d((0, self.window_length - len(xs[-1])), 0.)
                    # xs[-1] = pad(xs[-1])
                    del xs[-1]
                    del ys[-1]
                elif len(xs[-1]) < self.feature_window_size and not self.use_raw_audio:
                    # pad = torch.nn.ConstantPad1d((0, self.feature_window_size - len(xs[-1])), 0.)
                    # xs[-1] = pad(xs[-1].T).T
                    del xs[-1]
                    del ys[-1]
                if len(xs) > 0:
                    xs, ys = self._cat_and_encode_data(xs, ys)
                    yield xs, ys

    def _cat_and_encode_data(self, xs, ys):
        xs = torch.stack(xs, dim=0).to(device=self.device)
        ys = multilabel_onehot(ys, self.no_classes, device=self.device)
        return xs, ys

    def _iterate_over_stream(self, filename):

        current_observations = self.observations[self.observations['filename'] == filename]
        stream = librosa.stream(self.path_to_audio_files / filename,
                                block_length=1,
                                frame_length=self.window_length,
                                hop_length=self.hop_length)

        frame_start, frame_end = self.first_frame_start, self.first_frame_end
        for x in stream:
            y = self._get_overlapping_observations(current_observations,
                                                   frame_start, frame_end)['target'].unique()

            yield self._get_features(x, filename), y
            frame_start += self.hop_length
            frame_end += self.hop_length

    def _get_features(self, x, filename):
        if self.use_raw_audio:
            return x
        else:
            sr = self.sample_rates[filename]
            mfcc = librosa.feature.mfcc(y=x, sr=sr, n_mfcc=self.n_mfcc, hop_length=self.n_mfcc_hop_length)
            spectral_center = librosa.feature.spectral_centroid(
                y=x, sr=sr, hop_length=self.n_mfcc_hop_length
            )
            chroma = librosa.feature.chroma_stft(y=x, sr=sr, hop_length=self.n_mfcc_hop_length)
            spectral_contrast = librosa.feature.spectral_contrast(
                y=x, sr=sr, hop_length=self.n_mfcc_hop_length
            )
            return np.concatenate((mfcc, spectral_center, chroma, spectral_contrast)).T

    @staticmethod
    def _get_overlapping_observations(observations_df, frame_start, frame_end):
        return observations_df[((observations_df['Start (frame)'] >= frame_start)
                               & (observations_df['Start (frame)'] <= frame_end))
                               | ((observations_df['Stop (frame)'] >= frame_start)
                               & (observations_df['Stop (frame)'] <= frame_end))
                               | ((observations_df['Start (frame)'] <= frame_start)
                               & (observations_df['Stop (frame)'] >= frame_start))
                               | ((observations_df['Start (frame)'] <= frame_end)
                               & (observations_df['Stop (frame)'] >= frame_end))]

    @staticmethod
    def _map_features(x):
        if x.startswith('P, ') or x.startswith('S, '):
            return x[3:]
        return x


def read_datasets():
    """
    reads and preprocesses ntu dataset.
    """
    json_path = Path("./data/transcriptions")
    df = pd.read_csv("./data/audio_observations_v3.csv", sep=',', parse_dates=True)
    # Remove audio features
    df = df.loc[~df['Behavior'].isin(['Use of voice intonation to emphasise more important issues/topics',
                                      'Laughter']), :]

    # Remove features with not enough examples
    df = df.loc[
         ~df['Behavior'].isin(['Referring to bibliography, other researchers',
                               'Giving hints how to do something']), :]

    interesting_behaviors = [
        'Asking questions',
        'Giving questions to students: rhetorical, comprehension questions',
        'Organization: giving class outline, clearly indicating transition from one topic to another',
        'Active teacher stands by slides and explains them',
        'Summing up',
        'Session on tests',
        'questions'
    ]

    behaviors_mapping = {k: i for i, k in enumerate(interesting_behaviors)}
    return df, json_path, behaviors_mapping