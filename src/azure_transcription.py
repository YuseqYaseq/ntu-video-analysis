import azure.cognitiveservices.speech as speechsdk
import time
import json
from pathlib import Path
from tqdm import tqdm
from warnings import warn
import os

#speech_config = speechsdk.SpeechConfig(subscription=speech_key, region=service_region)
#speech_recognizer = speechsdk.SpeechRecognizer(speech_config=speech_config)


def speech_recognize_continuous_from_file(filename, speech_key, service_region, verbose):
    """performs continuous speech recognition with input from an audio file"""
    # <SpeechContinuousRecognitionWithFile>
    list_of_jsons = []
    speech_config = speechsdk.SpeechConfig(subscription=speech_key,
                                           region=service_region)
    speech_config.request_word_level_timestamps()
    audio_config = speechsdk.audio.AudioConfig(filename=filename)

    speech_recognizer = speechsdk.SpeechRecognizer(speech_config=speech_config,
                                                   audio_config=audio_config,
                                                   language='en-SG')

    done = False

    def stop_cb(evt):
        """callback that signals to stop continuous recognition upon receiving an event `evt`"""
        if verbose > 0:
            print('CLOSING on {}'.format(evt))
        nonlocal done
        done = True

    def append(evt):
        nonlocal list_of_jsons
        j = json.loads(evt.result.json)
        if j['RecognitionStatus'] not in ['Success', 'InitialSilenceTimeout']:
            warn(f'Something went wrong: {j}')
            with open('error.json', 'w') as error_file:
                json.dump(j, error_file)
            raise RuntimeError()
        list_of_jsons.append(j)

    # Connect callbacks to the events fired by the speech recognizer
    speech_recognizer.recognized.connect(append)
    speech_recognizer.session_stopped.connect(stop_cb)
    speech_recognizer.canceled.connect(stop_cb)
    if verbose > 0:
        speech_recognizer.session_started.connect(lambda evt: print('SESSION STARTED: {}'.format(evt)))
        speech_recognizer.session_stopped.connect(lambda evt: print('SESSION STOPPED {}'.format(evt)))
        speech_recognizer.canceled.connect(lambda evt: print(f'CANCELED {evt}, {evt._cancellation_details}'))
    if verbose > 1:
        speech_recognizer.recognizing.connect(lambda evt: print('RECOGNIZING: {}'.format(evt)))
        speech_recognizer.recognized.connect(lambda evt: print('RECOGNIZED: {}'.format(evt)))
        # stop continuous recognition on either session stopped or canceled events

    # Start continuous speech recognition
    speech_recognizer.start_continuous_recognition()
    while not done:
        time.sleep(.5)

    speech_recognizer.stop_continuous_recognition()
    return list_of_jsons, speech_recognizer


def process(files, save_to_path, speech_key, service_region, verbose=0):
    """
    Transcribes files and saves to save_to_path
    :param files: audio files for transcription
    :param save_to_path: results will be saved here
    :param speech_key: Azure transcription speech key (refer to Azure documentation)
    :param service_region: Azure transcription service region (refer to Azure documentation)
    """

    save_to_path = Path(save_to_path)

    for file in files:
        save_path = save_to_path / (Path(file).stem + "_unprocessed.json")
        if os.path.exists(save_path):
            continue
        list_of_jsons, sr = speech_recognize_continuous_from_file(file, speech_key, service_region, verbose)
        with open(save_path, 'w') as file_unprocessed:
            json.dump(list_of_jsons, file_unprocessed)
