import os
from pathlib import Path

import numpy as np
import pandas as pd
from math import ceil
from copy import deepcopy
from itertools import product
import pickle
import torch
from torch import nn
from typing import List, Dict
from warnings import warn
from hashlib import md5

from transformers import AutoModelForSequenceClassification, BertForSequenceClassification, \
    RobertaForSequenceClassification, XLNetForSequenceClassification


class TrueBatchSampler:
    """
    Reads from dataset once using slicing (on shuffle set to False) and
    list of indexes (on shuffle set to True) to speed up the processing.
    The dataset has to implement __len__ and __getitem__.
    """
    def __init__(self, dataset, batch_size, shuffle, seed=None):
        self.dataset = dataset
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.seed = seed
        if self.shuffle:
            np.random.seed(seed)
            self.idx = np.random.choice(range(len(self.dataset)), size=len(self.dataset), replace=False)
        self.i = 0

    def __iter__(self):
        self.i = 0
        if self.shuffle:
            np.random.seed(self.seed)
            self.idx = np.random.choice(range(len(self.dataset)), size=len(self.dataset), replace=False)
        return self

    def __next__(self):
        if self.i*self.batch_size < len(self.dataset):
            self.i += 1
            start = (self.i - 1) * self.batch_size
            end = self.i * self.batch_size
            if self.shuffle:
                if isinstance(self.dataset, pd.DataFrame):
                    return self.dataset.iloc[self.idx[start:end]]
                return self.dataset[self.idx[start:end]]
            else:
                if isinstance(self.dataset, pd.DataFrame):
                    return self.dataset.iloc[start:end]
                return self.dataset[start:end]
        else:
            raise StopIteration

    def __len__(self):
        return ceil(len(self.dataset) / self.batch_size)


def create_sequential_from_modules(input_size, output_size, modules=None,
                                   activation_function=nn.ReLU, output_function=None):
    if modules is None or len(modules) == 0:
        layers = [nn.Linear(input_size, output_size)]
    else:
        layers = [nn.Linear(input_size, modules[0]), activation_function()]
        for i in range(1, len(modules)):
            layers.extend([nn.Linear(modules[i - 1], modules[i]), activation_function()])
        layers.append(nn.Linear(modules[-1], output_size))
    if output_function is not None:
        layers.append(output_function())
    return nn.Sequential(*layers)


class ExperimentSetting:
    """
    Contains possible options of an experiment. Can be treated as a simple dict object.
    """
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def __copy__(self):
        return ExperimentSetting(**self.__dict__)

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            setattr(result, k, deepcopy(v, memo))
        return result

    def update(self, **kwargs):
        self.__dict__.update(**kwargs)
        
    def __contains__(self, item):
        return item in self.__dict__


def grid_search(grid: Dict[str, List]):
    """
    Iterate over all combinations of values in dict
    example:
    grid = {'k1': [1,2,3], 'k2': [4,5], 'k3': [6]}
    returns generator [{'k1': 1, 'k2': 4, 'k3': 6}, {'k1': 1, 'k2': 5, 'k3': 6},
                       {'k1': 2, 'k2': 4, 'k3': 6}, {'k1': 2, 'k2': 5, 'k3': 6],
                       ..., {'k1': 3, 'k2': 5, 'k3': 6}]
    """
    for current_values in product(*[i for i in grid.values()]):
        yield {key: current_values[i] for i, key in enumerate(grid.keys())}


def get_device():
    """
    Get best available torch device.
    """
    if torch.cuda.is_available():
        return torch.device('cuda')
    else:
        warn('Computing on cpu!')
        return torch.device('cpu')


def weighted_binary_cross_entropy(output, target, weights=None):
    """
    https://discuss.pytorch.org/t/solved-class-weight-for-bceloss/3114
    :param output:
    :param target:
    :param weights: lista [waga_klasy_0, waga_klasy_1] lub inaczej [liczba_elementów_klasy_1, liczba_elementów_klasy_0].
    :return:
    """
    # To fix NaN
    epsilon = 1e-15

    if weights is not None:
        assert len(weights) == 2

        loss = weights[1] * (target * torch.log(output + epsilon)) + \
            weights[0] * ((1 - target) * torch.log(1 - output + epsilon))
    else:
        loss = target * torch.log(output + epsilon) + (1 - target) * torch.log(1 - output + epsilon)

    return torch.neg(torch.mean(loss))


class NaNLossDetectedException(Exception):
    pass


def with_suffix(path, suffix):
    return path.parent / (path.stem + suffix)


def disc_cache(data_path, key=None):
    data_path = Path(data_path)

    def decorator(f):
        def inner(*args, **kwargs):
            if key is None:
                h = (f.__module__, f.__name__, args, kwargs)
            else:
                h = (f.__module__, f.__name__, key(*args, **kwargs))
            hashed_name = md5(pickle.dumps(h)).hexdigest()
            print("hashed_name: ", hashed_name)
            for file in os.listdir(data_path):
                file = Path(file)

                # Is this our file?
                if file.stem == hashed_name:
                    path = data_path / file
                    # How should we read this file?
                    if file.suffix == '.feather':
                        return pd.read_feather(path)
                    elif file.suffix == '.pt':
                        return torch.load(path)
                    elif file.suffix == 'npy':
                        return np.load(path)
                    else:
                        raise RuntimeError('Cached file extension is unrecognised!')

            # Result was not cached; we have to create it.
            result = f(*args, **kwargs)
            path = data_path / hashed_name
            if isinstance(result, (pd.DataFrame, pd.Series)):
                result.to_feather(with_suffix(path, '.feather'))
            elif isinstance(result, np.ndarray):
                np.save(with_suffix(path, '.npy'), result)
            else:
                torch.save(result, with_suffix(path, '.pt'))
            return result
        return inner

    return decorator


def get_preds(model, dataset, preprocess_data, used_classes, config, device):
    preds = []
    actuals = []
    for i, data in enumerate(dataset):
        x, actual = preprocess_data(data, config, device)
        if isinstance(model, (BertForSequenceClassification, RobertaForSequenceClassification,
                              XLNetForSequenceClassification)):
            pred = model(**x).logits
            pred = torch.sigmoid(pred)
        else:
            pred = model(x)
        pred = pred.reshape(-1, used_classes).detach().cpu()
        preds.append(pred)
        actuals.append(actual)
    return torch.cat(preds).type(dtype=torch.float32), torch.cat(actuals).type(torch.float32)


def get_metrics(preds, actuals, used_classes, threshold=0.5, reverse_classes=False):
    """
    Calculate metrics using predicted values and true values.
    """
    if not isinstance(preds, torch.Tensor):
        device = get_device()
        preds = torch.tensor(preds, device=device, dtype=torch.float32)
    if not isinstance(actuals, torch.Tensor):
        device = get_device()
        actuals = torch.tensor(actuals, device=device, dtype=torch.float32)

    tp, tn, fp, fn, a_, p_, r_, f1_ = [], [], [], [], [], [], [], []
    preds = preds.reshape(-1, used_classes)
    actuals = actuals.reshape(-1, used_classes)
    for selected_class in range(used_classes):
        preds_ = preds[:, selected_class] >= threshold
        actuals_ = actuals[:, selected_class]
        true_positive = torch.sum(preds_[actuals_ == 1])
        false_positive = torch.sum(preds_[actuals_ == 0])
        true_negative = torch.sum(~preds_[actuals_ == 0])
        false_negative = torch.sum(~preds_[actuals_ == 1])

        if reverse_classes:
            tmp = true_positive
            true_positive = true_negative
            true_negative = tmp

            tmp = false_negative
            false_negative = false_positive
            false_positive = tmp

        accuracy = (true_positive + true_negative).float() / (true_positive + true_negative + false_positive + false_negative).float()
        p = true_positive.float() / (true_positive + false_positive)
        r = true_positive.float() / (true_positive + false_negative)
        f1_score = 2 * (p * r) / (p + r)
        tp.append(true_positive.tolist())
        tn.append(true_negative.tolist())
        fp.append(false_positive.tolist())
        fn.append(false_negative.tolist())
        a_.append(accuracy.tolist())
        p_.append(p.tolist())
        r_.append(r.tolist())
        f1_.append(f1_score.tolist())
    return tp, tn, fp, fn, a_, p_, r_, f1_


def jaccard_index(s1, s2, return_tuple=False):
    """
    Calculate jaccard index
    :param s1: list of tuples of two ints representing timestamps
    :param s2: list of tuples of two ints representing timestamps
    :param return_tuple: on False return (A ^ B) / (A u B)
    on True return (A ^ B), (A u B)
    :return:
    """
    def _set_sum(start1, end1, start2, end2):
        """ returns sum if there is an overlap and None otherwise """
        if start2 <= start1 <= end2:
            return start2, max(end1, end2)
        if start1 <= start2 <= end1:
            return start1, max(end1, end2)
        return None  # separate sets

    def _set_intersection(start1, end1, start2, end2):
        """ returns intersection if there is an overlap and None otherwise """
        if start2 <= start1 <= end2:
            return start1, min(end1, end2)
        if start1 <= start2 <= end1:
            return start2, min(end1, end2)
        return None  # separate sets

    # Calculate A u B
    sum = []
    if isinstance(s1, np.ndarray) or isinstance(s2, np.ndarray):
        both_lists = np.concatenate((s1, s2))
    else:
        both_lists = s1 + s2
    for x, y in both_lists:
        matched_elem = False
        for i, (x2, y2) in enumerate(sum):
            set_sum = _set_sum(x, y, x2, y2)
            if set_sum is not None:
                sum[i] = set_sum
                matched_elem = True
                break
        if not matched_elem:
            sum.append((x, y))

    # join overlapping timestamps
    element_is_joined = [False for _ in sum]
    for i, (x, y) in enumerate(sum):
        if not element_is_joined[i]:
            for j, (x2, y2) in enumerate(sum):
                if element_is_joined[j] or i == j:
                    continue
                set_sum = _set_sum(x, y, x2, y2)
                if set_sum is not None:  # overlap is found
                    sum[j] = set_sum
                    element_is_joined[i] = True
                    break

    sum_ = 0
    for (x, y), is_joined in zip(sum, element_is_joined):
        if not is_joined:
            sum_ += y - x

    if sum_ == 0:
        raise ValueError('Division by zero')

    # calculate A ^ B
    intersection = 0
    for x, y in s1:
        for x2, y2 in s2:
            set_intersection = _set_intersection(x, y, x2, y2)
            if set_intersection is not None:
                intersection += set_intersection[1] - set_intersection[0]

    if return_tuple:
        return intersection, sum_
    else:
        return intersection / sum_
