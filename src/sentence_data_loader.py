from functools import reduce
import json
import os
from sklearn.model_selection import train_test_split
from tqdm import tqdm
from warnings import warn
import pandas as pd
import numpy as np

from src.util import disc_cache


def _get_overlapping_observations(observations_df, start, end):
    return observations_df[((observations_df['Start'] >= start)
                            & (observations_df['Start'] <= end))
                           | ((observations_df['Stop'] >= start)
                              & (observations_df['Stop'] <= end))
                           | ((observations_df['Start'] <= start)
                              & (observations_df['Stop'] >= start))
                           | ((observations_df['Start'] <= end)
                              & (observations_df['Stop'] >= end))]


def _merge(elems, merge_threshold, num_behaviors):
    new_df = pd.DataFrame()
    lengths = elems['DisplayText'].str.split(' ')\
        .apply(lambda x: 0 if isinstance(x, float) else len(x))
    elems['DisplayText'] = elems['DisplayText'].apply(lambda x: '' if isinstance(x, float) else x)
    groups = []

    current_group_length = 0
    current_group_id = 0
    last_filename = elems.loc[0, 'filename']
    for i, vals in enumerate(elems.values):
        current_filename = vals[0]
        if current_group_length > merge_threshold or current_filename != last_filename:
            current_group_id += 1
            current_group_length = 0
            last_filename = current_filename
        groups.append(current_group_id)
        current_group_length += lengths[i]
    elems['group'] = groups

    elems['text'] = elems.groupby('group')['DisplayText']\
        .transform(lambda x: ' '.join(x))
    new_df[['filename', 'text']] = elems.drop_duplicates(subset=['group'], ignore_index=True)[['filename', 'text']]
    new_df['Start'] = elems.groupby('group')['Start'].min()
    new_df['Stop'] = elems.groupby('group')['Stop'].max()
    new_df = pd.DataFrame(new_df)
    new_df[list(range(num_behaviors))] = elems.groupby('group')[list(range(num_behaviors))].max()
    return new_df


def _add_labels_based_on_annotation(transcription_df, annotation_df, behaviors_mapping, file_name):
    transcription_df["Offset"] = transcription_df["Offset"] / 10_000_000
    transcription_df["Duration"] = transcription_df["Duration"] / 10_000_000
    transcription_df["Start"] = transcription_df["Offset"]
    transcription_df["Stop"] = transcription_df["Start"] + transcription_df["Duration"]
    transcription_start_stop = transcription_df[["Start", "Stop"]].values
    transcription_labels = np.zeros((len(transcription_df), len(behaviors_mapping)))

    # Label transcribed elements based on df
    for i, start_stop in enumerate(transcription_start_stop):
        overlapping_observations = _get_overlapping_observations(annotation_df, start_stop[0], start_stop[1])
        identified_behaviors = overlapping_observations['Behavior'].unique()
        idx = []
        for behavior in identified_behaviors:
            idx.append(behaviors_mapping[behavior])
        transcription_labels[i, idx] = 1.0
    return np.concatenate((
            np.array([file_name for _ in transcription_labels])[:, np.newaxis],
            transcription_df["DisplayText"].values[:, np.newaxis],
            transcription_start_stop,
            transcription_labels), axis=1)


@disc_cache(data_path='./cache',
            key=lambda df, json_path, merge_threshold, behaviors_mapping, test_size, seed:
            (merge_threshold, test_size, seed))
def create_text_df(df, json_path, merge_threshold, behaviors_mapping, test_size, seed):
    """
    Builds and caches the NTU dataset from transcriptions and behaviors observations.
    :param df: audio_observations_v3 dataframe.
    :json_path: path to transcriptions.
    :merge_threshold: if there are less than merge_threshold words in a sentence it is merged with another sentence.
    :behaviors_mapping: dict which maps behavior string to one of [0,1,2,3,4,5,6]
    :test_size: test_size% will go to df_test
    :seed: seed for RNG
    :returns: df_train, df_test.
    Two created dataframes with columns 'text' and [0, 1, 2, 3, 4, 5, 6] refering to behavior classes.
    """
    elems = []
    for file_name in tqdm(os.listdir(json_path), desc='building NTU dataset'):
        file = open(json_path / file_name, 'r')
        file = json.load(file)
        video_name = file_name[:-17]
        df_ = df[df["Media file mapped"] == video_name]
        if len(df_) == 0:
            warn(f"No description found for video {video_name}.")
        transcription = pd.DataFrame(file)
        transcription = _add_labels_based_on_annotation(transcription, df_, behaviors_mapping, video_name)
        elems.append(transcription)

    elems = np.concatenate(elems, axis=0)
    elems = pd.DataFrame(elems)
    elems.columns = ['filename', 'DisplayText', 'Start', 'Stop'] + list(range(len(behaviors_mapping)))
    elems[6] = elems.apply(lambda x: max(x[0], x[1]), axis=1)
    # Merge sentences when necessary
    elems = _merge(elems, merge_threshold, num_behaviors=len(behaviors_mapping))

    # Train test split based on filename
    files = elems['filename'].unique()
    train_files, test_files = train_test_split(files, test_size=test_size, random_state=seed)
    train_elems = elems[elems['filename'].isin(train_files)]
    test_elems = elems[elems['filename'].isin(test_files)]

    return train_elems, test_elems


def isnan(x):
    return x != x


def ted_data_loader(path):
    df = pd.read_feather(path)
    df.rename(columns={'automatic transcription': 'text', '6': 6}, inplace=True)
    return df

