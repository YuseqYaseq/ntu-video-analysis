from transformers import XLNetModel, XLNetConfig, XLNetTokenizer, XLNetForSequenceClassification
import torch
from torch import nn

from src.util import create_sequential_from_modules


def preprocess_data(x, config, device):
    if config.task == 'simplified':
        y = torch.tensor(x[config.full_task_num_of_classes].values, device=device, dtype=torch.float32)
    elif config.task == 'full':
        y = torch.tensor(x[list(range(config.full_task_num_of_classes))].values, device=device, dtype=torch.float32)
    else:
        raise RuntimeError('Task unknown!')
    return x['text'].tolist(), y
    #return [i['text'] for i in x],\
    #       torch.tensor([[label for j, label in enumerate(i['labels'])] for i in x],
    #                    device=device, dtype=torch.float32)


def tokenize(tokenizer, x, device):
    return tokenizer(x, return_tensors='pt', padding=True, truncation=True).to(device)


class XLNetClassifier(nn.Module):
    def __init__(self, model_name, config, xlnet_config):
        super().__init__()
        self.xlnet = XLNetModel.from_pretrained(model_name, config=xlnet_config)
        self.xlnet.requires_grad = False
        if config.task == 'simplified':
            output_size = 1
            output_fun = nn.Sigmoid
        elif config.task == 'full':
            output_size = config.full_task_num_of_classes
            output_fun = None
        else:
            raise RuntimeError('Unknown config.task!')
        self.l = create_sequential_from_modules(config.bert_hidden_size,
                                                output_size,
                                                modules=config.bert_classifier_modules,
                                                activation_function=config.bert_classifier_activation_function,
                                                output_function=output_fun)

    def forward(self, x):
        x = self.xlnet(**x).last_hidden_state
        x = x.mean(dim=1)
        x = self.l(x)
        return x

    def unfreeze_bert(self):
        self.roberta.requires_grad = True


def get_xlnet(config, model_name, device):
    model_config = XLNetConfig.from_pretrained(model_name)
    #model_config.hidden_size = config.bert_hidden_size
    model = XLNetClassifier(model_name, config, model_config).to(device)
    tokenizer = XLNetTokenizer.from_pretrained(model_name, config=model_config)
    return model, tokenizer
