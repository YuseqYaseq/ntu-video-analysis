from transformers import BertModel, BertConfig, BertTokenizer
import torch
from torch import nn

from src.util import create_sequential_from_modules


def preprocess_data(x, config, device):
    if config.task == 'simplified':
        y = torch.tensor(x[config.full_task_num_of_classes].values, device=device, dtype=torch.float32)
    elif config.task == 'full':
        y = torch.tensor(x[list(range(config.full_task_num_of_classes))].values, device=device, dtype=torch.float32)
    else:
        raise RuntimeError('Task unknown!')
    return x['text'].tolist(), y
    #return [i['text'] for i in x],\
    #       torch.tensor([[label for j, label in enumerate(i['labels'])] for i in x],
    #                    device=device, dtype=torch.float32)


def tokenize(tokenizer, x, device):
    return tokenizer(x, return_tensors='pt', padding=True, truncation=True).to(device)


class BertClassifier(nn.Module):
    def __init__(self, model_name, config, bert_config):
        super().__init__()
        self.bert = BertModel.from_pretrained(model_name, config=bert_config)
        #self.bert.requires_grad = False
        if config.task == 'simplified':
            output_size = 1
            output_fun = nn.Sigmoid
        elif config.task == 'full':
            output_size = config.full_task_num_of_classes
            output_fun = None
        else:
            raise RuntimeError('Unknown config.task!')
        self.l = create_sequential_from_modules(config.bert_hidden_size,
                                                output_size,
                                                modules=config.bert_classifier_modules,
                                                activation_function=config.bert_classifier_activation_function,
                                                output_function=output_fun)

    def forward(self, x):
        #x = self.tokenizer(x, return_tensors='pt', padding=True, truncation=True).to(device)
        x = self.bert(**x).pooler_output
        x = self.l(x)
        return x

    def unfreeze_bert(self):
        self.bert.requires_grad = True


def get_bert(config, model_name, device):
    #model_config = BertConfig.from_pretrained(model_name)
    #model_config.hidden_size = config.bert_hidden_size
    #model = BertClassifier(model_name, config, model_config).to(device)
    #tokenizer = BertTokenizer.from_pretrained(model_name, config=model_config)
    #return model, tokenizer
    from transformers import AutoModelForSequenceClassification, AutoTokenizer
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    if config.task == 'simplified':
        return AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=1).to(device), tokenizer
    else:
        return AutoModelForSequenceClassification.from_pretrained(model_name,
                                                                  num_labels=config.full_task_num_of_classes).to(device), tokenizer


def get_bert_tokenizer(config, model_name):
    model_config = BertConfig.from_pretrained(model_name)
    model_config.hidden_size = config.bert_hidden_size
    tokenizer = BertTokenizer.from_pretrained(model_name, config=model_config)
    return tokenizer
