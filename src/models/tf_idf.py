from pathlib import Path
from uuid import uuid4

import pandas as pd
import torch
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression

from src.util import ExperimentSetting


def tf_idf_pipeline(df_train: pd.DataFrame, df_test: pd.DataFrame, config: ExperimentSetting):
    """
    Pipeline for tf-ifd method. Trains on df_train and returns test results from df_test.
    :param df_train: dataframe containing training data.
    Contains at least columns: 'text' and either 6 (if config.task == 'full')
    or 1, 2, 3, 4, 5 (if config.task == 'simplified')
    :param df_test: dataframe containing test data. Has the same format as df_train.
    :param config: ExperimentSetting object. Refer to config.py for all fields which this object should contain.
    :returns: tuple (true positive, true negative, false positive, false negative,
                     accuracy, precision, recall, f1 score) calculated on the df_test.
    """
    def positive_negative_to_metrics(true_positive, false_positive, true_negative, false_negative):
        accuracy = (true_positive + true_negative).float() / (
                true_positive + true_negative + false_positive + false_negative).float()
        p = true_positive.float() / (true_positive + false_positive)
        r = true_positive.float() / (true_positive + false_negative)
        f1_score = 2 * (p * r) / (p + r)
        return true_positive.tolist(), false_positive.tolist(), true_negative.tolist(), \
               false_negative.tolist(), accuracy.tolist(), p.tolist(), r.tolist(), f1_score.tolist()

    def get_metrics(preds: torch.Tensor, actual: torch.Tensor, logistic: bool, threshold=0.5):
        assert len(preds.shape) == 1, "Predictions have to be 1-dimensional!"
        assert len(actual.shape) == 1, "True values have to be 1-dimensional!"
        if logistic:
            preds_ = preds >= threshold
            true_positive = torch.sum(preds_[actual == 1])
            false_positive = torch.sum(preds_[actual == 0])
            true_negative = torch.sum(~preds_[actual == 0])
            false_negative = torch.sum(~preds_[actual == 1])
            return positive_negative_to_metrics(true_positive, false_positive, true_negative, false_negative)
        else:
            mae = torch.mean(torch.abs(preds - actual)).tolist()
            mse = torch.mean((preds - actual) * (preds - actual)).tolist()
            return mae, mse

    if config.save_path is None:
        config.save_path = Path(f'results/{config.selected_model}_{uuid4()}.pt')
    tfidf_vect = TfidfVectorizer(ngram_range=config.tf_idf_ngram_range,
                                 max_features=config.tf_idf_max_features,
                                 token_pattern=config.tf_idf_token_pattern)
    X_train_tfidf = tfidf_vect.fit_transform(df_train['text'])
    len(tfidf_vect.vocabulary_)

    lr = LogisticRegression(solver=config.tf_idf_solver,
                            penalty=config.tf_idf_penalty,
                            random_state=config.seed,
                            max_iter=config.tf_idf_max_iter)

    if config['task'] == 'full':
        from sklearn.multioutput import MultiOutputClassifier
        lr = MultiOutputClassifier(lr)
        _y = df_train[[0, 1, 2, 3, 4, 5]]
    else:
        _y = df_train[[6]].squeeze()
    lr.fit(X_train_tfidf, _y)
    X_test_tfidf = tfidf_vect.transform(df_test['text'])
    y_pred = lr.predict(X_test_tfidf)

    if config['task'] == 'full':
        m = [get_metrics(torch.tensor(y_pred[:, i].squeeze(), dtype=torch.float32),
                         torch.tensor(df_test[i].values.squeeze(), dtype=torch.float32),
                         logistic=True) for i in [0, 1, 2, 3, 4, 5]]
    else:
        m = get_metrics(torch.tensor(y_pred, dtype=torch.float32),
                        torch.tensor(df_test[6].values, dtype=torch.float32),
                        logistic=True)
    torch.save({
        'model': lr,
        'config': config,
        'metrics': m,
    }, config.save_path)
    return m
