from torch import nn

from src.util import ExperimentSetting

config = ExperimentSetting()

# common
config.seed = 42
config.lr = 1e-5
config.batch_size = 4
config.weight_decay = 0.0
config.epochs = 5
config.lr_scheduler_factor = 0.1
config.lr_scheduler_patience = 4
config.lr_scheduler_min_lr = 1e-7
config.test_size = 0.1
config.sample_size = 1.0
config.save_path = None

config.merge_threshold = 8
config.task = 'simplified'  # 'simplified' or 'full'
config.dataset = 'ted'  # 'ted' or 'ntu'
config.full_task_num_of_classes = 6
config.save_checkpoint = False


config.selected_model = 'BERT'  # or 'xlnet' or 'roberta' or 'tfidf'

# BERT/xlnet/roberta config
config.bert_hidden_size = 768
config.bert_classifier_modules = [1000]
config.bert_classifier_activation_function = nn.LeakyReLU
config.bert_unfreeze_at = 35

# tf-idf config
config.tf_idf_solver = 'saga'
config.tf_idf_penalty = 'l1'
config.tf_idf_max_iter = 999
config.tf_idf_ngram_range = (1, 2)
config.tf_idf_max_features = 10_000
config.tf_idf_token_pattern = r"(?u)\b\w\w+\b|!|\?|\"|'"
