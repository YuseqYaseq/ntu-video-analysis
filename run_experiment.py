from copy import copy
from sklearn.model_selection import train_test_split

from config import config
from src.data_loader import read_datasets
from src.models.tf_idf import tf_idf_pipeline
from src.pipeline import training_pipeline
from src.sentence_data_loader import create_text_df, ted_data_loader
from src.util import grid_search


example_grid = {
    'merge_threshold': [3],
    'epochs': [1],
    'selected_model': ['tfidf'],
    'dataset': ['ted'],
    'task': ['simplified'],
    'sample_size': [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
}


def run_experiment(grid):
    """
    Perform hyperparameter optimization on selected task/dataset.
    Iterates over all combinations of hparams and trains model on each.
    :param grid: grid used for grid search. Refer to example_grid how it should look like.
    Possible options are in config.py. If a parameter is not provided a default value from config.py is used.
    """
    for i, params in enumerate(grid_search(grid)):
        # Wstaw parametry z grid searcha do config
        current_config = copy(config)
        current_config.update(**params)

        if current_config['dataset'] == 'ntu':
            try:
                df, json_path, behaviors_mapping = read_datasets()
            except FileNotFoundError:
                # Maybe results are cached. In that case we do not need these.
                df = None
                json_path = None
                behaviors_mapping = None
            df_train, df_test = create_text_df(df, json_path,
                                               merge_threshold=current_config.merge_threshold,
                                               behaviors_mapping=behaviors_mapping,
                                               seed=current_config.seed,
                                               test_size=current_config.test_size)
        elif current_config['dataset'] == 'ted':
            df = ted_data_loader('./data/ted/final_dataset_from_video.feather')
            df_train, df_test = train_test_split(df, test_size=0.1, random_state=1)
        else:
            raise RuntimeError("Unknown dataset!")

        df_train = df_train.sample(frac=current_config.sample_size)

        if current_config.selected_model.lower() != 'tfidf':
            results = training_pipeline(df_train, df_test, current_config)
        else:
            tp, tn, fp, fn, a, p, r, f1 = tf_idf_pipeline(df_train, df_test, current_config)
            print(f"true positive: {tp}; true negative: {tn}; false positive: {fp}; false negative: {fn};"
                  f"Accuracy: {a}; Precision: {p}; Recall: {r}; F1 score: {f1}")


if __name__ == '__main__':
    run_experiment(example_grid)
