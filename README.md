# NTU video analysis
## Reprocibility
To reproduce results download code from this repository and follow provided instruction.  
NTU dataset:
* The NTU dataset is proprietary. For access contact anna.wroblewska@mini.pw.edu.pl and insert provided files into the _data_ folder.\
TED Talks dataset:
* The dataset is already available in the repository. However if you wish to recreate it follow these instructions.
* To download audio files go to https://drive.google.com/drive/folders/1clqw9izazxafPDuIekXQYYdI-J42VvCR
* Open folder _VIDEO_. Important note: the _AUDIO_ folder contains data which is not matched with the human transcription so it shouldn't be used because WER transcription method will not perform well.
* download all files from folder _VIDEO_ and the _TED_Talk.csv_ file.
* go to the main repository, launch jupyter notebook and open _ted_transcription.ipynb_
* Provide the following parameters:
    - _speech_key_ - key to Azure Speech recognition service (https://azure.microsoft.com/en-us/services/cognitive-services/speech-services/).
    - _service_region_ - region of the Azure Speech recognition service.
    - _main_path_ - path to downloaded video files.
    - _df_path_ - path to the downloaded csv file.
* Launch all cells in the notebook and wait untill the process is complete.
* Open _ted_text_merge.ipynb_ and launch all cells.

To train models import run_experiment from run_experiment.py and provide values for grid search. Example grid is shown in run_experiment.py.
Available options are provided in config.py.
```
from run_experiment import run_experiment, example_grid
run_experiment(example_grid)
```

## Code structure
The following list describes all elements of the code structure
* _data_ - contains datasets. Insert NTU dataset files here.
* _preliminary work_ - contains audio preprocessing modules and utilities used in the preliminary work.
* _results_ - default save path of trained models.
* _src_ - contains source code.
* _config.py_ - contains default configuration of the experiments.
* _results_visualisation.ipynb_ - contains visualisations of results.
* _results_summary.feather_ - contains results in feather format (https://arrow.apache.org/docs/python/feather.html).
* _run_experiment.py_ - main script. Launches grid search and finds the best hyperparameters for the selected task.
* _ted_text_merge.ipynb_ - contains WER merge algorithm.
* _ted_transcription.ipynb_ - contains transcription algorithm.
