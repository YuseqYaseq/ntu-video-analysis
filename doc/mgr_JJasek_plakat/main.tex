%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MI2 DataLab Poster
% LaTeX Template
% Version 1.0 (21/11/2018)
%
% Created by:
% Alicja Gosiewska & Tomasz Mikołajczyk
% MI2 DataLab, Faculty of Mathematics and Information Technology, Warsaw University of Technology
% http://mi2.mini.pw.edu.pl
% 
% Further modified by:
% Józef Jasek
%
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------------------------------------------------------------------------------
%	PACKAGES
%------------------------------------------------------------------------------------

%\usepackage[backend=bibtex,sorting=none,style=trad-abbrv,citestyle=numeric]{biblatex}
\documentclass[final]{beamer}
\setbeamertemplate{caption}[numbered]
\usetheme{MI2} 
%\usepackage[scale=1.4, size = custom, width = 70,7, height = 100 ,orientation=portrait]{beamerposter} %B1 size
 \usepackage[scale=1.4, size = a0 ,orientation=portrait]{beamerposter} % A0 size
\usepackage{graphicx} 
\usepackage[utf8]{inputenc}

\usepackage[misc]{ifsym}
\usepackage{multicol}
\usepackage{subcaption}
\usepackage{booktabs}
\usepackage[export]{adjustbox}
\usepackage{caption}
%\usepackage[backend=bibtex, sorting=none]{biblatex}

%------------------------------------------------------------------------------------
%	CONFIGURATION
%------------------------------------------------------------------------------------


\newlength{\twocolwid}
\newlength{\threecolwid}
\newlength{\sepwid}
\setlength{\twocolwid}{0.455\paperwidth} % Width of one column in two-column setup
\setlength{\sepwid}{0.024\paperwidth} % Separation width between columns


\setbeamersize{text margin left=27mm,text margin right=27mm} 
\setlength{\topmargin}{0mm}

\newcommand{\Tsup}[1]{%
  \texorpdfstring{\textsuperscript{#1}}{}%
}

%------------------------------------------------------------------------------------
%	TITLE SECTION 
%------------------------------------------------------------------------------------

\title{\huge{Recognition of Behavioural \\ Features Relevant to Didactics \\ Based on Recorded Lectures}} 

\author{
\large{
Thesis author: Józef Jasek\\
Thesis supervisor: Anna Wróblewska, PhD
}} % Poster Authors


%------------------------------------------------------------------------------------
%	POSTER 
%------------------------------------------------------------------------------------


\begin{document}

\begin{frame}[t] % The whole poster is enclosed in one frame

\maketitle

\begin{columns}[t] % The whole poster consists of two major columns

%----------------------
% THE FIRST MAJOR COLUMN
%----------------------
\begin{column}{\twocolwid} 



%----------------------
%	INTRODUCTION
%----------------------

\begin{block}{Introduction}

This work focuses on recognition of predefined didactic behaviours in a dataset of video lectures recorded at Nanyang Technological University (NTU), Singapore and a dataset comprised of TED talks videos. We compare audio-based models (\textbf{Wav2Letter}~\cite{collobert2016wav2letter}, \textbf{M5}~\cite{very_deep_convolutional_neural_networks_for_raw_waveforms}) trained on audio data extracted from the datasets and text-based models (\textbf{BERT}~\cite{BERT}, \textbf{TF-IDF}~\cite{tf_idf}, \textbf{Contextual Bandits}~\cite{contextual_bandit_algorithm} implemented by VowpalWabbit, \textbf{RoBERTa}~\cite{roberta}, \textbf{fasttext}~\cite{fasttext} and \textbf{XLNet}~\cite{xlnet}) trained on audio transcription. This work is part of the ALE (Automatic Lecture Evaluation) project which aims to create a system for automatic evaluation of lecturers to improve their teaching abilities based on the assessment.
\end{block}


\begin{block}{NTU Dataset}
We selected a subset of the dataset containing videos in English. It means that the NTU dataset is comprised of 160 videos with a cumulative length of about 250 hours. Human annotators annotated each video. In this work, we focused only on audio-based behaviours (e.g. \textit{Asking questions}) and discarded other behaviours (e.g. \textit{Eye contact}). Each behaviour corresponds to one of the predicted classes.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.8\textwidth, left]{./images/behaviors_bar.png}
  \caption{Number of occurences of each behaviour (NTU dataset)}
  \label{fig:sub1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\textwidth, right]{./images/duration_box.png}
  %\includegraphics[height=15cm, right]{./images/duration_box.png}
  \caption{Duration of occurences of each behaviour (NTU dataset)}
  \label{fig:sub2}
\end{subfigure}
\label{fig:test}
\end{figure}

%\begin{figure}
%\includegraphics[width=\linewidth]{./images/behaviors_bar.png}
%\caption{Number of occurences of each behaviour (NTU dataset)}
%\end{figure}


Behaviours \textit{R} and \textit{GH} are discarded due to a low number of occurrences. Behaviours \textit{L} and \textit{U} are discarded because the information necessary for prediction is lost during transcription. Audio transcription is performed using the Azure Speech Recognition service. Each sample corresponds to one fragment of continuous speech. A sample belongs to a behaviours' class in the NTU dataset if an annotation of this behaviour with overlapping timestamps exists. It means that one sample can belong to any number of behaviours. Two tasks are solved on this dataset:
\begin{itemize}
    \item \textit{Questions} - a binary classification problem - does a sample belong to at least one of \textit{AQ} and \textit{GQ}?
    \item \textit{Full task} - a multilabel multiclass classification problem - prediction of all behaviours.
\end{itemize}
\end{block}

\begin{block}{TED Talks dataset}
In the case of the TED talks dataset, only a human transcription is given. So only the \textit{Asking questions} behaviour is created by classifying a sentence as belonging to this class if a question mark is present in the human transcription. To prepare samples for text-based modelling, Azure transcription is matched with human transcription by minimizing the Word Error Rate metric. This dataset contains 350 videos. Binary classification of \textit{Asking questions} is performed on this dataset. Only text-based models are trained on this dataset due to unavailable audio timestamps of the manual transcription.

\end{block}

\begin{block}{Results}
Results show that text-based models performed much better despite the propagation of ASR transcription error. Noticeably all results on the TED talks dataset are much higher than their corresponding results on the NTU dataset. 

\begin{table}
    \centering
    \begin{tabular}{lllll}
    \textbf{Model} & \textbf{Accuracy} & \textbf{Precision} & \textbf{Recall} & \textbf{F1 score} \\  \hline
    BERT & 0.807 & 0.565 & 0.702 & 0.626 \\
    RoBERTa & 0.817 & 0.583 & 0.712 & 0.641 \\
    XLNet & 0.583 & 0.325 & \textbf{0.758} & 0.455 \\
    TF-IDF & \textbf{0.887} & \textbf{0.828} & 0.599 & \textbf{0.695} \\
    \end{tabular}
    \caption{Results for the \textit{Asking questions} prediction task (TED talks dataset)}
    \label{tab:ted_results}
\end{table}

\end{block}



\end{column}


%----------------------
% THE SECOND MAJOR COLUMN
%----------------------
\begin{column}{\twocolwid}


\begin{block}{}
We may also notice that transfer learning based on state-of-the-art text models such as BERT does not perform significantly better than simple, linear models such as TF-IDF.



\begin{table}
    \centering
    \begin{tabular}{lllllll}
    \textbf{Model} & \textbf{AQ} & \textbf{GQ} & \textbf{O} & \textbf{S} & \textbf{AT} & \textbf{SU} \\ \hline
    BERT & 0.29 & \textbf{0.27} & 0.03 & 0.08 & 0.31 & 0.02 \\
    VowpalWabbit & \textbf{0.32} & \textbf{0.27} & 0.17 & \textbf{0.37} & 0.52 & 0.02 \\
    FastText & 0.04 & 0.03 & 0.03 & 0.16 & 0.43 & 0.0 \\
    RoBERTa & 0.28 & 0.25 & \textbf{0.20} & 0.20 & 0.45 & \textbf{0.06} \\
    XLNet & 0.29 & 0.24 & 0.19 & 0.21 & 0.45 & 0.02 \\
    TF-IDF & 0.08 & 0.09 & 0.05 & 0.21 & \textbf{0.63} & 0.0 \\
    \end{tabular}
    \centering
    \caption{\label{tab:full_task}F1 scores on all text features (NTU dataset).}
\end{table}


\begin{table}
\centering
\begin{tabular}{lllll}
\textbf{Model} & \textbf{Accuracy} & \textbf{Precision}
& \textbf{Recall} & \textbf{F1} \\
\hline
Wav2Letter & \textbf{0.906} & \textbf{1.00} & 0.00 & 0.00 \\
M5 & 0.896 & 0.364 & 0.003 & 0.006 \\
BERT & 0.831 & 0.332 & 0.453 & 0.383 \\
VowpalWabbit & 0.757 & 0.481 & 0.387 & \textbf{0.429} \\
FastText & 0.745 & 0.442 & 0.321 & 0.373 \\
RoBERTa & 0.116 & 0.116 & \textbf{1.00} & 0.207 \\
XLNet & 0.690 & 0.177 & 0.459 & 0.255 \\
TF-IDF & 0.884 & 0.461 & 0.100 & 0.164 \\
\end{tabular}
\caption{Results for the \textit{Questions} prediction task (NTU dataset)}
\label{tab:questions_only}
\end{table}

\begin{figure}
  \centering
  
  \captionsetup{justification=centering,margin=2cm}
  \includegraphics[width=0.4\textwidth]{./images/korelacja.png}
  \caption{Correlation between cumulative duration \\of behaviour and its top predicted F1 score}
  \label{fig:sub2}
\end{figure}


\end{block}

\begin{block}{Conclusions}
Text-based models significantly outperformed audio-based models. They were cheaper and faster to train due to significantly reduced dataset size (200GB vs 5MB) and achieved much better F1 scores (<1\% vs 43\%). However, they do not apply to all behaviours, e.g. \textit{Laughter} or \textit{Use of voice intonation...}. Additionally, scores achieved on the TED talks dataset are much higher than their corresponding scores on the NTU dataset. It suggests that the NTU manual annotation may not be as reliable as the manual transcription provided by the TED conference.

\end{block}

%----------------------
%	REFERENCES
%----------------------


\begin{block}{References}

\nocite{*} % Insert publications even if they are not cited in the poster
\bibliographystyle{abbrv}
\footnotesize{\bibliography{references}}
  
\end{block}




%----------------------
%	CONTACT INFORMATION
%----------------------

\begin{block}{}

\begin{minipage}{0.84\linewidth}
\normalsize{
\begin{itemize}
\item \normalsize{Instructions on how to reproduce these results are available at: \url{https://gitlab.com/YuseqYaseq/ntu-video-analysis}}\vspace{0.5cm}
\end{itemize}
}
\end{minipage} \hfill



\end{block}

\end{column}



\end{columns}
\end{frame}
\end{document}